from flask import Flask, jsonify, request
from flask_restful import Resource, Api
import sys
import glob
import serial

app = Flask(__name__)
api = Api(app)
arduino = ''
connected = False
romValue = {}

@app.route("/getPorts")
def get_Ports():
    result = []
    if sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        ports = glob.glob('/dev/tty[A-Za-z]*')
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return jsonify(result)

@app.route("/connectPort", methods=['POST'])
def connect_Port():
     if request.method == 'POST':

         recieve_data = request.get_json(silent=True)
         portID = recieve_data["portID"]
         baudrate = recieve_data["baudRate"]
         #Command:
         #curl -i -X POST -H "Content-Type: application/json" -d '{"portID":"ttyACM0", "baudRate":"115200"}' http://127.0.0.1:5000/connectPort

         #portID = request.form
         #curl -X POST http://127.0.0.1:5000/connectPort -F portID=kjbjkasv -F hkj=sfsf -F gg=343
         arduino = serial.Serial(portID, 115200)
         connected = False
         while not connected:
             if arduino.readline().strip().decode("ascii") == "waiting for connection!!":  # strip used to strip off newline and carriage return and decode is used to decode the bytes into ascii
                 connected = True
                 arduino.write(b'!#!')
                 heartBeat = arduino.readline().strip().decode("ascii")
                 while not heartBeat.startswith("Connected!"):
                     heartBeat = arduino.readline().strip().decode("ascii")
                 global FlipmouseVersion
                 FlipmouseVersion = heartBeat.split(':')[1]
                 romValue = get_rom_value_from_arduino()
                 status =jsonify({'portID':portID, 'version':FlipmouseVersion, 'status':'Connected', 'romData':romValue})
                 return status
            else:
                status =jsonify({'portID':'unknown', 'version':'unknown', 'status':'disconnected', 'romData':'unknown'})
                return status


def get_rom_value_from_arduino():
    """Method to fethch the ROM value from the flipMouse
    """
    romVal = {}
    arduino.write(b'*#*rv')
    while True:
        ip = arduino.readline().strip().decode("ascii")
        if ip.startswith("*#*|") and ip.endswith("|*#*"):
            ip = ip.strip("*#*").strip("|")
            break
    if ip:
        ip = ip.split('|')
        i = 0
        while (i < len(ip)):
            romVal.update({ip[i]:ip[i+1]})
            i = i+2
    return romVal

if __name__ == '__main__':
    app.run(debug=True)
