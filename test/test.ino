/*  Firmware for  FlipMouse
    Copyright (C) 2018  ICFOSS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//Pressure Sensor MPXV7007GP connected to pin 10 
//Mode Switch connected to D7
//Buzer conneted to D4
int b= A0, f=A3, r=A2,l=A1;
char mode=0;
int front, midfront = 24, back, midback = 25, left, midleft=17, right, midright=192, Fsensitivity = 2;
long unsigned int timer1,timer2;
bool pressed = false;
float pressure;int midpressure=0,Psensitivity=2;
void beep(int a =-1)
{
  if (a==0)
  {
    tone(4, 1000, 100);
    delay(200);
    tone(4, 1000, 100);
  }
  if (a==1)
   tone(4, 3300, 500);
   
  if (a == -1)
    tone(4, 2300, 50); 
}


void setup() 
{
  Serial.begin(9600);
  pinMode(7,INPUT);
  pinMode(4,OUTPUT);
  Serial.print("\nIntialised!\nEnter Mode 1:Values 2:Calibrate");
  while(mode==0)
  {
    if ((timer2-millis())>2000)
      {
        Serial.print("\nEnter Mode 1:Values 2:Calibrate");
        timer2=millis();
      }
    if (Serial.available())
    {
        mode = Serial.read();
        Serial.print("Mode Entered:");Serial.println(mode);
    }
  }
  beep();
  
}

void loop() 
{
  pressure = analogRead(A10);
  if (mode=='1')
  {
    disply();
  }

  else
  {
    calib();
  }
}


void disply()
{
  Serial.print("F:");Serial.print(analogRead(f));Serial.print(" B:");Serial.print(analogRead(b));Serial.print(" L:");
  Serial.print(analogRead(l));Serial.print(" R:");Serial.print(analogRead(r));Serial.print(" Pressure = ");Serial.println(pressure);
  delay(200);
}


void calib()
{
  int front = analogRead(f), back = analogRead(b), right = analogRead(r), left = analogRead(l);
  pressure = map(pressure,510,530,0,10);

  front = map (front,100,1024,0,50);
  back  = map (back,100,1024,0,50);
  left  = map (left,100,1024,0,50);
  right = map (right,100,1024,0,50);

  Serial.print("F:");Serial.print(front);Serial.print(" B:");Serial.print(back);Serial.print(" L:");
  Serial.print(left);Serial.print(" R:");Serial.print(right);Serial.print(" Pressure = ");Serial.println(pressure);
  delay(200);

  if (front < midfront-Fsensitivity)
  {
    if (right<midright-Fsensitivity)
      Serial.println("Front right");
    else if (left < midleft-Fsensitivity)
      Serial.println("Front left");
    else
      Serial.println("Frontt");  
  }

  else if (back < midback-Fsensitivity)
  {
    if (right<midright-Fsensitivity)
      Serial.println("Back right");
    else if (left < midleft-Fsensitivity)
      Serial.println("Back left");
    else
      Serial.println("Back");  
  }
  else if (left < midleft-Fsensitivity)
      Serial.println("Left");
  else if (right < midright-Fsensitivity)
      Serial.println("Right");    
    
  if (digitalRead(7)==HIGH)
      {
        if(!pressed)
        {
          beep();
          Serial.println("Mode Switch!");
          timer1 = millis();
          pressed = true;
        }
        if(pressed)
        {
          if ((millis()-timer1)>2300)
          {
            Serial.println("\nCalibrating!");
            beep(0);
            delay(500);
            midfront = front;
            midback = back;
            midleft = left;
            midright = right;
            Serial.print("Calibrated at mF:");Serial.print(midfront);Serial.print(" mB:");Serial.print(midback);Serial.print(" mL:");Serial.print(midleft);Serial.print(" mR:");Serial.println(midright);        
            beep(1);
            delay(500);
            pressed = false;
          }
        }
      }
     else
      pressed = false;
    if ((pressure< (midpressure-Psensitivity))||(pressure>(midpressure+Psensitivity)))
     {Serial.print("Pressure = ");Serial.println(pressure);}
}


