import sys
import serial
import glob
import time
from tabCompletion import Tabcomplete
import os
import json


romValue = {}
FlipmouseVersion = 0
class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[41m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    ITALICS = '\033[3m'
    UNDERLINE = '\033[4m'
    BGYellow = '\033[43m'
    YELLOW = '\033[0;33m'
    END = '\033[0m'
    BLINK = '\033[5m'
    GREY = '\033[90m'

def portScan():
    """Method to scan the USB ports and list them, so that the user can pick
    the port to which the device is Connected
    Returns:
     -> Returns the port to which the device is connected
    """
    if len(sys.argv) > 1:
        port  = sys.argv[1]
        try:
            s = serial.Serial(port)
            s.close()
        except (OSError):
            print("Invalid Port Number! Incase of unknown port Do not give an arguments so as to intitate auto scan")
            sys.exit()
        except KeyboardInterrupt:
            exit(0)

    else:
        if sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            ports = glob.glob('/dev/tty[A-Za-z]*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        print("Active Ports: ")
        i=1
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                if port.split('/')[2].startswith("ttyACM"):
                    print(i,". "+colors.GREEN,port,colors.END)
                else:
                     print(i,". "+colors.GREY,port,colors.END)
                result.append(port)
                i = i+1
            except (OSError, serial.SerialException):
                pass
            except KeyboardInterrupt:
                exit(0)
        select = int(input("Please Select the port: "))
        select = select - 1
        port = result[select]
        return port
# **************************************************************************************************************
def get_and_execute_commmand(arduino):
    """Method to get the command from the user and exceute them. The module also
    imports tabcompletion modules which would help the user to set the parameters
    for flipmouse
    """
    global romValue
    autoComplete = Tabcomplete("commands.json")
    changes_made = False
    while True:
        input_command = autoComplete.getip(colors.GREEN+"Enter Command>> "+colors.END,"commands",dynamicUpdation = 'n')

        if input_command == "calibrate_sip_and_puff":
            arduino.write(b'*#*sp')
            transfer_control_to_arduino()
            changes_made = True

        elif input_command == "clear_rom":
            arduino.write(b'*#*ro')
            transfer_control_to_arduino()

        elif input_command == "update_rom":
            oldRomVal = dict(romValue)
            print("\t",colors.OKBLUE+"Wait till the audio from the device stops..."+colors.END)
            changes_made = False
            arduino.write(b'*#*ur')
            transfer_control_to_arduino()
            romValue = get_rom_value_from_arduino()
            if romValue == oldRomVal:
                print("\t\t"+colors.WARNING+"Couldn't update ROM values...Please try again"+colors.END)
            else:
                print("\t\t"+colors.GREEN+"ROM Values Updated"+colors.END)


        elif input_command == "reset_device":
            arduino.write(b'*#*re')
            print("Now your arduino will restart as a mouse!")
            exit()

        elif input_command == "test_sensor_data":
            arduino.write(b'*#*te')
            test_raw_sensor()


        elif input_command == "sip_hold_function":
            get_value = input(colors.OKBLUE+"\tEnter:\n\t1-> Scroll Function\n\t2)Volume Control\n\t\t:"+colors.END)
            if get_value == '1':
                    arduino.write(b'*#*pu1')
                    changes_made = True
            elif get_value == '2':
                    arduino.write(b'*#*pu2')
                    changes_made = True
            else:
                print("\n\tWrong Command!")

        elif input_command == "calibrate_force_sensors":
            print(colors.OKBLUE+"\tCalibrating your FliMouse please wait till the audio playback stops...")
            print(colors.GREY+"\tYou can also do the same by holding the mode button for more than 3 seconds."+colors.END)
            arduino.write(b'*#*fc')
            transfer_control_to_arduino()

        elif input_command == "set_sensitivity_force_sensor":
            print("\tPresent Value: "+colors.YELLOW,romValue["deadZone"],colors.END)
            get_value = input(colors.OKBLUE+"\t   Enter a sensitivy value between 0 and 10 (10 is the least sensitive): "+colors.END)
            if int(get_value) >= 0 and int(get_value) <= 10:
                arduino.write(b'*#*vaf'+get_value.encode())
                changes_made = True
            else:
                print("\t"+colors.YELLOW+"Value not updated (value noe inside the desired limit)"+colors.END)

        elif input_command == "set_sensitivity_pressure_sensor":
            print("\tPresent Value: "+colors.YELLOW,romValue["Psensitivity"],colors.END)
            get_value = input(colors.OKBLUE+"\tEnter a sensitivy value between 0 and 5: "+colors.END)
            if int(get_value) > 0 and int(get_value) <= 5:
                arduino.write(b'*#*vap'+get_value.encode())
                changes_made = True
            else:
                print("\t"+colors.YELLOW+"Value not updated (value noe inside the desired limit)"+colors.END)

        elif input_command == "set_minimum_speed":
            print("\tPresent Value: "+colors.YELLOW,int(romValue["speedDelay"])/5,colors.END)
            get_value = input(colors.OKBLUE+"\tEnter a minimum range between 0 and 5 (5 beeing the slowest): "+colors.END)
            if int(get_value) >= 0 and int(get_value) <= 5:
                arduino.write(b'*#*vas'+get_value.encode())
                changes_made = True
            else:
                print("\t"+colors.YELLOW+"Value not updated (value noe inside the desired limit)"+colors.END)

        elif input_command == "set_minimum_pixel_shift":
            print("\tPresent Value: "+colors.YELLOW,romValue["hardcodeSpeed"],colors.END)
            get_value = input(colors.OKBLUE+"\tEnter a minimum pixek shift range between 0 and 5 : "+colors.END)
            if int(get_value) >= 0 and int(get_value) <= 5:
                arduino.write(b'*#*vah'+get_value.encode())
                changes_made = True
            else:
                print("\t"+colors.YELLOW+"Value not updated (value noe inside the desired limit)"+colors.END)

        elif input_command == "set_dynamic_speed_offset":
            print("\tPresent Value: "+colors.YELLOW,romValue["speedShifter"],colors.END)
            get_value = input(colors.OKBLUE+"\tEnter a dynamic speed offset between 0 and 10 (0 - speed increase if Off): "+colors.END)
            if int(get_value) >= 0 and int(get_value) <= 10:
                arduino.write(b'*#*vad'+get_value.encode())
                changes_made = True
            else:
                print("\t"+colors.YELLOW+"Value not updated (value noe inside the desired limit)"+colors.END)

        elif input_command == "set_mode":
            print("\tPresent Value: "+colors.YELLOW,romValue["Mode"],colors.END)
            get_value = int(input(colors.OKBLUE+"\tEnter a the mode of device:\n\t\t1)Mouse mode\n\t\t2)Keyboard Arrowkey mode  "+colors.END))-1
            if get_value >= 0 and get_value <= 1:
                arduino.write(b'*#*vam'+get_value.encode())
                changes_made = True
            else:
                print("\t"+colors.YELLOW+"Value not updated (value noe inside the desired limit)"+colors.END)

        elif input_command.startswith("set_function_aux"):
            get_value = input(colors.OKBLUE+"\tEnter any of the following options\n\t a- Left Click\n\t b- Right Click\n c- BackSpace\n\t\t: "+colors.END).lower()
            if input_command.endswith("aux1"):
                command = '*#*a1'
            if input_command.endswith("aux2"):
                command = '*#*a2'
            if input_command.endswith("aux3"):
                command = '*#*a3'
            valid_values = ["a", "b", "c"]
            if get_value in valid_values:
                arduino.write(command.encode()+get_value.encode())
                changes_made = True
            else:
                 print("Not a valid input!")


        elif input_command.startswith("save_settngs"):
            if changes_made:
                while True:
                    temp = input("It seems that there are some value changes made and device rom is not updated, before saving the setting should we update the device rom (y/n)?: ").lower()
                    if temp == 'y':
                        arduino.write(b'*#*ur')
                        transfer_control_to_arduino()
                        time.sleep(8)
                        break

                    elif temp == 'n':
                        break
            if input(colors.YELLOW+colors.ITALICS+"Do you wish to save the current setting to a file (y/n)? "+colors.END).lower() == 'y':
                save_custom_settings()

        elif input_command == "exit":
            if changes_made:
                while True:
                    temp = input("It seems that there are some value changes made, Before exiting shoud we save the changes (y/n)?: ").lower()
                    if temp == 'y':
                        arduino.write(b'*#*ur')
                        transfer_control_to_arduino()
                        time.sleep(8)
                        break

                    elif temp == 'n':
                        print("Exiting....")
                        break

            print("FlipMouse will now work as mouse. Exiting application")
            arduino.write(b'!#!')
            exit()

        else:
            outputString = "\n"+colors.YELLOW+colors.UNDERLINE+"Help"+colors.END+"\n"

            outputString = outputString + colors.BOLD + "  calibrate_sip_and_puff:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to calibrate the sip and puff threshold for the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  clear_rom:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to clear the calibration data saved in FlipMouse Memory.\n"+colors.END

            outputString = outputString + colors.BOLD + "  update_rom:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to update the new calibrated value to the FlipMouse Memory.\n"+colors.END
            outputString = outputString + colors.GREY + "  \t      " + colors.END + colors.HEADER+"Note: "+colors.END+colors.GREY+"Please update"
            outputString = outputString+" rom before exiting the program if you have changed any calibrations \n"+colors.END

            outputString = outputString + colors.BOLD + "  reset_device:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to flipmouse into mouse mode.\n"+colors.END

            outputString = outputString + colors.BOLD + "  test_sensor_data:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to print sensor data from arduino to terminal screen.\n"+colors.END

            outputString = outputString + colors.BOLD + "  sip_hold_function:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to set the sip hold function of the fliMouse..\n"+colors.END

            outputString = outputString + colors.BOLD + "  calibrate_force_sensors:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to calibrate the force sensors for the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  set_sensitivity_force_sensor:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to set the force sensor sensitivity of the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  set_sensitivity_pressure_sensor:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to set the pressure sensor sensitivity of the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  set_minimum_pixel_shift:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to set the minimum pixel shift of mouse cursor the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  set_minimum_speed:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to set the minimum speed of the cursor in the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  set_dynamic_speed_offset:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to switch on and off the dynamic speed increase in the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  set_function_aux1:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to set the function of aux port 1 in the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  set_function_aux2:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to set the function of aux port 2 in the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  set_function_aux3:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to set the function of aux port 3 in the flipmouse.\n"+colors.END

            outputString = outputString + colors.BOLD + "  set_mode:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to set the mode of the FlipMouse\n\t1)Mouse Mode: The lipstick will mve the mouse cursor."
            outputString = outputString + colors.BOLD + "  \n\t2)Arrowkey Mode: The lipstick will act as up, down, right and left keys of your keyboard"+colors.END+"\n"

            outputString = outputString + colors.BOLD + "  save_settngs:"+colors.END+"\n"
            outputString = outputString + colors.GREY + "  \t Command to save the present flipMouse setting in to your computer.\n"+colors.END
            print(outputString)

def transfer_control_to_arduino():
    """Method to transfer output control to arduino. Here the output from arduino
    is directly printed on to the terminal.
    """
    iteration = 0
    while True:
        ip = arduino.readline().strip().decode("ascii")

        if ip.startswith("CommandExecuted!") and iteration != 0:
            print(colors.GREY+"\tDone\n"+colors.END)
            arduino.reset_input_buffer()
            break

        elif ip != "" and not ip.startswith("CommandExecuted!"):
            print("\t",ip)
            iteration = iteration + 1

        ip = ""
    arduino.reset_input_buffer()
    arduino.flushInput()
    return True

def test_raw_sensor():
    """Method to transfer output control to arduino and print raw value from the
    sensors
    """
    def printbar(front,back,left,right,pressure):
        move_cursor('e',0)
        print(colors.GREY,"\tFront: ",front," Back: ",back," Left: ",left," Right: ",right," Pressure: ",pressure,colors.END)
        divident = 51
        front = int(front)/divident
        back = int(back)/divident
        left = int(left)/divident
        right = int(right)/divident
        limit = 20
        move_cursor('e',0)
        print("\t\tFront ",colors.GREEN,"|"*int(front),colors.GREY+"|"*(limit-int(front)),colors.END,"\n")
        move_cursor('e',0)
        print("\tLeft ", colors.GREEN, "|"*int(left), colors.GREY+"|"*(limit-int(left)),colors.END, end = '')
        print(' '*5,"Right ",colors.GREEN,"|"*int(right),colors.GREY,"|"*(limit-int(right)),colors.END,"\n")
        move_cursor('e',0)
        print("\t\tBack ",colors.GREEN,'|'*int(back),colors.GREY,"|"*(limit-int(back)),colors.END,)
        move_cursor('e',0)
        print("\t"," "*4,"Pressure ",colors.OKBLUE,'|'*int(pressure.split('.')[0]),colors.GREY,"|"*(limit-int(pressure.split('.')[0])),colors.END,)
        move_cursor('u',7)

    def move_cursor(direction,pos):
        """Method to print out ascii escape codes for moving manipulating the output
        Requires two parameters:
         -> direction : which denotes what to do.
                        'u' - > move up the cursor
                        'd' - > move down the cursor
                        'r' - > move the cursor to the right
                        'l' - > move the cursor to the left
                        'e' - > erase the present line
                        's' - > move up the cursor to the starting of the line
         -> pos  : denotes the number of times ascii escape has to be printed.
        """
        asci = ""
        if direction == 'u':
            asci = u"\u001b[1A"
        elif direction == 'd':
            asci = u"\u001b[1B"
        elif direction == 'r':
            asci = u"\u001b[1C"
        elif direction == 'l':
            asci = u"\u001b[1D"
        elif direction == 'e':
            asci = u"\033[2K"
        elif direction == 's':
            asci = u"\u001b[1000D"
        for i in range(pos):
            sys.stdout.write(asci)
        sys.stdout.flush()

    iteration = 0
    while True:
        try:
            ip = arduino.readline().strip().decode("ascii")

            if ip.startswith("CommandExecuted!") and iteration != 0 :
                print(colors.GREY+"\tDone\n"+colors.END)
                arduino.reset_input_buffer()
                break
            elif ip != "" and not ip.startswith("CommandExecuted!"):
                iteration = iteration + 1
                if ip.startswith("Raw front"):
                    ip = ip.split(":")
                    printbar(ip[1],ip[3],ip[5],ip[7],ip[9])
                #print("\t",ip)
            ip = ""
        except KeyboardInterrupt:
            arduino.write(b'end')
            move_cursor('d',7)
            pass
    arduino.reset_input_buffer()
    arduino.flushInput()
    return True

def get_rom_value_from_arduino():
    """Method to fethch the ROM value from the flipMouse
    """
    romVal = {}
    arduino.write(b'*#*rv')
    while True:
        ip = arduino.readline().strip().decode("ascii")
        if ip.startswith("*#*|") and ip.endswith("|*#*"):
            ip = ip.strip("*#*").strip("|")
            break
    if ip:
        ip = ip.split('|')
        i = 0
        while (i < len(ip)):
            romVal.update({ip[i]:ip[i+1]})
            i = i+2
    print(colors.OKBLUE+" Done!"+colors.END)
    #print(colors.GREY,romVal,colors.END)
    return romVal


def save_custom_settings():
    customSettings = get_rom_value_from_arduino()
    filename = input("\t\t"+colors.GREEN+"->"+colors.OKBLUE+" Enter the filename in which the file should be saved:\n\t\t\t"+colors.END)
    with open("custom_setting_"+filename+".json", 'w+') as outfile:
        json.dump(customSettings, outfile)

def load_custom_settings():
    print(colors.GREY+"Searching for custom settings in present working directory"+colors.END)
    full_files  = os.listdir(os.getcwd())
    files = []
    for temp in full_files:
        if temp.startswith("custom_setting") and  temp.endswith(".json"):
            files.append(temp)
    print(files)
    files = {"files":files}
    fileNameAutoComplete = Tabcomplete(files)
    fileName = fileNameAutoComplete.getip(colors.GREEN+"\t\tEnter FileName>> "+colors.END,"files",dynamicUpdation = 'n')
    data = {}
    print(os.getcwd()+str(fileName))
    if os.path.exists(os.getcwd()+'/'+str(fileName)):
        print("Path found")
        data = json.load(str(os.getcwd()+'/'+str(fileName)))

    print("Json: ",data)

if __name__ == '__main__':
        try:
            #load_custom_settings()
            # exit()
            port = portScan()
            print(colors.OKBLUE+"Selected port:"+colors.END," ", port, colors.GREY+"\n\tWaiting for handshake\n\tPlease Press the reset button on your device press hold the mode button incase if the device doesnt respond"+colors.END)
            arduino = serial.Serial(port, 115200)
            connected = False
            while not connected:
                if arduino.readline().strip().decode("ascii") == "waiting for connection!!":  # strip used to strip off newline and carriage return and decode is used to decode the bytes into ascii
                    print(colors.YELLOW+colors.ITALICS+"Heart beat recieved!.."+colors.END,end = ' ')
                    connected = True
                    arduino.write(b'!#!')
                    heartBeat = arduino.readline().strip().decode("ascii")
                    while not heartBeat.startswith("Connected!"):
                        heartBeat = arduino.readline().strip().decode("ascii")
                    global FlipmouseVersion
                    FlipmouseVersion = heartBeat.split(':')[1]
                    print("\n\t\t\tFlipMouse Firmware Version: "+colors.OKBLUE+str(FlipmouseVersion)+colors.END)
                    print(colors.GREY+colors.ITALICS+"Fetching data from FlipMouse Memory...."+colors.END, end = " ")
                    romValue = get_rom_value_from_arduino()
                    if input(colors.YELLOW+colors.ITALICS+"Do you wish to save the current setting to a file (y/n)? "+colors.END).lower() == 'y':
                        save_custom_settings()
                    print(colors.GREEN+"Connected!"+colors.END)

            while connected:# while conection is esablished with arduino
                get_and_execute_commmand(arduino)


        except serial.serialutil.SerialException:  # to check if the device is unplugged or diconnected
            print(colors.WARNING+"Device not connected or is unplugged"+colors.END)
        except (UnicodeDecodeError):
            print(colors.BGYellow+"Sync Error \nPlease Reset your device and run again"+colors.END)
        except KeyboardInterrupt:
            pass
        arduino.write(b'!#!')
        print(colors.GREY+"\nclosing Application!"+colors.END)
