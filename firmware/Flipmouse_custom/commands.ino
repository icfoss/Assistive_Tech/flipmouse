char charUp (char c) {
  if ((c >= 'a') && (c<='z'))  
    c=c-'a'+'A';
  return(c);
}

void parseByte (int newByte)  // parse an incoming commandbyte from serial interface, perform command if valid
{
   static uint8_t readstate=0;
   static uint8_t cmdlen=0;
  
      switch (readstate) {
        case 0: 
                if (charUp(newByte)=='A') readstate++;
             break;
        case 1: 
                if (charUp(newByte)=='T') readstate++; else readstate=0;
            break;
        case 2: 
                if ((newByte==13) || (newByte==10))  // AT reply: "OK" 
                {  Serial.println(F("OK"));  readstate=0; }
                else if (newByte==' ') { cmdlen=0; readstate++; } 
                else goto err;
            break;
        case 3: 
                if (charUp(newByte=='I'))   
                  readstate++;
                  else if(charUp(newByte=='L')){Serial.println("Load all values");readstate=0;}    
                  else if(charUp(newByte=='E')){Serial.println("End report");readstate=0;  }  
                   else if(charUp(newByte=='S')){Serial.println("End report");readstate=0;  }  
                   break;   
            
         case 4:
              if(charUp(newByte=='D')) Serial.println("Indian Flipmouse v2.81");
              else if(charUp(newByte=='L')){ Serial.println("List IR");readstate=0; }
                  break;   
        default: err: Serial.println('?');readstate=0;
   }
}

