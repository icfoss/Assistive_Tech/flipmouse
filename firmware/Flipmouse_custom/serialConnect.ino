
/**
   To connect send the text "!#!"
   To send commands after connecting ->> *#*_ _ _ _
   3rd and 4th bit
      a1 -> Auxilary Port 1
      a2 -> Auxilary Port 2
      a3 -> Auxilary Port 3
      sp -> Sipp puff calib
      ro -> rom clear
      ur -> update ROM
      re -> reset device
      te -> test sensor data
      pu -> sip hold function setting
      fc -> Force Calibration
      va -> variable direct entry
      rv -> raw-Value output

   5th bit if variable direct entry
      f -> force sensor sensitivity
      p -> pressure sensor sensitivity
      h -> minimum pixel shift
      s -> minimum speed shift
      d -> dynamic speed shift
      m -> set mode (keyboard arrow mode or mouse)
      a -> arrow delay speed
      k -> arrow key sensitivity


   5th bit if Auxilary
      a -> Left Click
      b -> Right Click
      c -> Backspace

   5th bit if Sip Hold function
      0-> Scroll
      1-> volume mute
      2-> volume up


/*******************************************************************************
  Function Name: SerialConnect
  parameters: nil
  Function:
        It is invoked to intiate the serial connection between the host program in
your computer and the FlipMouse. The explanation for the sequential working of
the function can be seen here
[https://gitlab.com/icfoss/Assistive_Tech/flipmouse/tree/master/firmware/Flipmouse#serial-connect-mode]

*******************************************************************************/
void SerialConnect()
{
  beep('r');
  Serial.begin(115200);
  Serial.print("Connect..");
  beep('e');
  while (Serial.readString() != "!#!")
    Serial.println("waiting for connection!!");
  Serial.println("Connected!|FlipMouse: v1.0");
  beep('k');
  String ip = "";
  while (1)
  {
    ip = Serial.readString();
    if (ip.startsWith("*#*"))
      executeCommand(ip);
    if (ip.startsWith("!#!"))
      break;
  }
  Serial.end();
}

/*******************************************************************************
  Function Name: executeCommand
  parameters: string variable
  Function:
      The function helps to parse the string into commands to be executed to
  update the values recieved from the host program in the computer.
*******************************************************************************/
void executeCommand(String ip)
{
  if (ip.substring(3, 5) == "ro")                 //ROM Clear
  {
    Serial.println("Clearing Rom!.......");
    writeRom('e');
  }
  else if (ip.substring(3, 5) == "ur")                 //update Rom
  {
    Serial.println("Updating Rom!.......");
    writeRom('c');
    delay(1000);
  }
  else if (ip.substring(3, 5) == "re")            //Reset Device
  {
    Serial.println("Resetting Device..Please wait!");
    delay(1000);
    resetFunc();
  }
  else if (ip.substring(3, 5) == "sp")            //SIP calibrarion
  {
    Serial.print("Old values mid->"); Serial.print(midpressure); Serial.print(" Sensitivity-> "); Serial.println(Psensitivity);
    Serial.println("Sip Puff Calibration!\n Please donot sip or puff into the tube for 5 seconds!");
    delay(2800);
    midpressure = analogRead(pin_pressure_sensor);
    Serial.println("please puff the maximum into the tube for 5 seconds");
    delay(3000);
    pressuremax = analogRead(pin_pressure_sensor);
    Serial.println("please sip the maximum into the tube for 5 seconds");
    delay(3000);
    pressuremin = analogRead(pin_pressure_sensor);

    printIt("pressuremax", pressuremax);printIt("new_midpressure:", midpressure);printIt("pressuremin: ", pressuremin,1);
    Psensitivity = (pressuremax-midpressure>midpressure-pressuremin)?(midpressure-pressuremin)/2:(pressuremax-midpressure)/2;
    Psensitivity = map(Psensitivity,0,Psensitivity*2,1,5);
    midpressure = map(midpressure, pressuremin, pressuremax, 0, 10);
    printIt("Calibration Finished max-> ",pressuremax); printIt(" mid-> ",midpressure);printIt(" min-> ",pressuremin);printIt(" Sensitivity-> ",Psensitivity,1);
  }

  else if (ip[3] == 'a')
  {
    int temp = 0;
    switch (ip[5])
    {
      case 'a': temp = 1; //left click
                break;
      case 'b': temp = 2; //right click
                break;
      case 'c': temp = 3; //Backspace
                break;
    }
    switch(ip[4])
    {
      case '1': faux1 = temp;
                break;
      case '2': faux2 = temp;
                break;
      case '3': faux3 = temp;
                break;
    }
  }

   else if (ip.substring(3, 5) == "pu")
    fSip_Hold = ip[5];


  else if (ip.substring(3, 5) == "rv")
  {
    getRomValues();
  }

  else if (ip.substring(3, 5) == "va")
  {
    char choice = ip[6];
    switch(ip[5])
    {
      case 'f': deadZone = ip[6] - '0';  //0-9
                break;
      case 'p': Psensitivity = ip[6] - '0'; //0-9
                break;
      case 'h': hardcodeSpeed = (ip[6]-'0' >= 0)? ip[6]-'0': 0; //only positive integers 0-9
                break;
      case 's': speedDelay = ((ip[6]-'0'>=0) && (ip[6]-'0'<=10) )? (ip[6]-'0')*5: 0; //0-9
                break;
      case 'd': speedShifter = ip[6] - '0'; //0-9
                break;
      case 'm': keyboardMode = ip[6] - '0';
                break;
      case 'a': arrowKeyDelay = (ip[6] - '0')*100;
                break;
      case 'k': arrowSensitivity = ip[6] - '0';
                break;

    }
  }

  else if (ip.substring(3, 5) == "fc")
      calibrateForceSensors();

  else if (ip.substring(3,5) == "te")
      {
        while (!(Serial.readString()).startsWith("end"))
          {
            getRawValues(true);
            delay(50);
          }
        Serial.println("Displaying Raw value finished");
      }

  else
    Serial.println(ip.substring(3, 5));
 beep('k');
  Serial.println("CommandExecuted!");
}
