
/*
struct essentials
{
  int frontmid, backmid, leftmid, rightmid, deadZone;
  int pressuremid, pressureSensitivity, pressuremax,pressuremin;
  int a1, a2, a3, sip_Hold, hardcodeSpeed, speedDelay, speedShifter;
  int keyboardMode, arrowKeyDelay, arrowSensitivity;
};
*/


void readRom()
{
  /*
    In oder to check if the data in the ROM is valid, we will check to see if
    first value in the address 0 is 107 . If not, then we will intialise the
    essential data with the preset values.
  */
  essentials initdata;
  if (EEPROM.read(romAddrSet) == 107)
  {
    EEPROM.get(romAddrData, initdata);
    midfront = initdata.frontmid;
    midback = initdata.backmid;
    midright = initdata.rightmid;
    midleft = initdata.leftmid;
    deadZone = initdata.deadZone;
    midpressure = initdata.pressuremid;
    Psensitivity = initdata.pressureSensitivity;
    pressuremax = initdata.pressuremax;
    pressuremin = initdata.pressuremin;
    faux1 = initdata.a1;
    faux2 = initdata.a2;
    faux3 = initdata.a3;
    hardcodeSpeed = initdata.hardcodeSpeed;
    speedShifter = initdata.speedShifter;
    speedDelay = initdata.speedDelay;
    fSip_Hold = initdata.sip_Hold;
    keyboardMode= initdata.keyboardMode;
    arrowKeyDelay= initdata.arrowKeyDelay;
    arrowSensitivity= initdata.arrowSensitivity;
  }
  else
  {
    beep('i');
    initdata.frontmid = midfront = 16;
    initdata.backmid = midback = 49;
    initdata.leftmid = midleft = 16;
    initdata.rightmid = midright = 42;
    initdata.deadZone = deadZone = 2;
    initdata.pressuremid = midpressure = 0;
    initdata.pressureSensitivity = Psensitivity = 5;
    initdata.pressuremax = 593;
    initdata.pressuremin = 480;
    initdata.a1 = initdata.a2 = initdata.a3 = initdata.sip_Hold = 0;
    initdata.hardcodeSpeed = 1;
    initdata.speedDelay = 0;
    initdata.speedShifter = 0;
    initdata.keyboardMode = keyboardMode = 0;
    initdata.arrowKeyDelay = arrowKeyDelay = 500;
    initdata.arrowSensitivity = arrowSensitivity = 3;
    faux1 = faux2 = faux3 = fSip_Hold = 0;
    hardcodeSpeed = 1;
    EEPROM.write(romAddrSet, 107);
    EEPROM.put(romAddrData, initdata);
  }
}

void writeRom(char choice)
{
  if (choice == 'c')              // c -> calibrate mode
  {
    essentials initdata;
    initdata.frontmid = midfront;
    initdata.backmid = midback;
    initdata.leftmid = midleft;
    initdata.rightmid = midright;
    initdata.deadZone = deadZone;
    initdata.pressuremid = midpressure;
    initdata.pressureSensitivity = Psensitivity;
    initdata.pressuremax = pressuremax;
    initdata.pressuremin = pressuremin;
    initdata.a1 = faux1;
    initdata.a2 = faux2;
    initdata.a3 = faux3;
    initdata.sip_Hold = fSip_Hold;
    initdata.hardcodeSpeed = hardcodeSpeed;
    initdata.speedDelay = speedDelay;
    initdata.speedShifter = speedShifter;
    initdata.keyboardMode = keyboardMode;
    initdata.arrowKeyDelay = arrowKeyDelay;
    initdata.arrowSensitivity = arrowSensitivity;
    EEPROM.write(romAddrSet, 107);
    EEPROM.put(romAddrData, initdata);
    beep('i');
  }

  if (choice == 'e')              // e -> Erase ROM
  {
    beep('i');
    delay(300);
    beep('e');
    for (int i = 0 ; i < EEPROM.length() ; i++)
    {
      EEPROM.write(i, 0);
    }
    beep('e');
    delay(300);
    beep('i');
  }
}

void getRomValues()
{
  readRom();
  printIt("*#*|MidFront|",midfront);
  printIt("|MidBack|",midback);
  printIt("|MidRight|",midright);
  printIt("|MidLeft|",midleft);
  printIt("|deadZone|",deadZone);
  printIt("|midpressure|",midpressure);
  printIt("|Psensitivity|",Psensitivity);
  printIt("|pressuremax|",pressuremax);
  printIt("|pressuremin|",pressuremin);
  printIt("|faux1|",faux1);
  printIt("|faux2|",faux2);
  printIt("|faux3|",faux3);
  printIt("|hardcodeSpeed|",hardcodeSpeed);
  printIt("|speedDelay|",speedDelay);
  printIt("|fSip_Hold|",fSip_Hold);
  printIt("|speedShifter|",speedShifter);
  printIt("|Mode|",keyboardMode);
  printIt("|arrowKeyDelay|",arrowKeyDelay);
  printIt("|arrowSensitivity|",arrowSensitivity);
  Serial.println("|*#*");
}
