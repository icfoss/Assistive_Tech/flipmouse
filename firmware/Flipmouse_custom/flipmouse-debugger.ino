/*  Firmware for  FlipMouse
    Copyright (C) 2018  ICFOSS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#include "Mouse.h"
#include "Keyboard.h"
#include "EEPROM.h"
#include "Wire.h"
#include "math.h"


#define MOUSE_ACCELDELAY  20 
#define THRESHOLD         10

int x, y;
int hardcodeSpeed, buttondelay = 10, speedDelay, speedShifter;

       
uint8_t moveXcnt=0;       
uint8_t moveYcnt=0; 
uint8_t cnt2=0;  

uint8_t threshold =10;  

  
/*
Declare the pin number on which the force sensors and pressure sensors are connected to.
Also declare the pins to which aux1, aux2, aux3, modeSelector and buzzer is connected.
*/
int pin_back = A3, pin_front = A0, pin_right = A1, pin_left = A2, pin_pressure_sensor = A10;
int pin_aux1 = 6, modeSelector = 7, pin_aux2 = 8, pin_aux3 = 9, buzzer = 4;
char mode = 0;
String data ="start";
/*
Declare the front,back,left and right values that will control the trajectory
of the mouse cursor. And also declare the variables for storing the calculated
pressure values.
*/
int front, midfront, back, midback, left, midleft, right, midright, deadZone;
float pressure; int midpressure, Psensitivity, pressuremax,pressuremin;
bool sip = false, puff = false; // Global variable to store the present state of sip and puff
/*
Timer variable declaration for monitoring the duration of actiaons performed
So as to implement multipile functionalities for particular actions.
*/
long unsigned int timer1, timer2;
int inByte=0;



/*
 Global variable for
*/
bool pressed_mode_selector = false, pTrigger = false, scrollMode = false, pressureLock = false, controlTransfer = true;
// Variavle that store the functions assigned for various aux ports and pressure sip.
int faux1, faux2, faux3, fSip_Hold;
// Global variables to store the I2C addresses present in the bus during bootup
int i2cAddr[10];bool i2cPresent = false;

// Global Variable to set the joystick onto keyboard arrow mode
int keyboardMode, arrowKeyDelay = 500, arrowSensitivity =3;

// Variable for handling ROM data
int romAddrSet = 0, romAddrData =1;


//Function Prototype
void executeCommand(String);
void getRawValues(bool);
void getDirectionPressureval(int &xe, int &ye);
void beep(char);
void printIt(const char* ip, int &data,bool newLine = 0);
/*
 The structure to be initialised on boot. The values for the data members are
 fetched from ROM.
 values:
    *mid                -> middle values of each force and pressure sensors
                          (calculated during the calibration process).
    deadZone            -> the deadzone for each force sensor (set by the user
                           through adjusting sensivtivity).
    pressureSensitivity -> The sensitivity of the pressure sensor (set by the user)
    pressuremax/min     -> The max value of the pressure sensor (set during the
                           sip and puff calibration)
    a*                  -> The functon value of each aux port (set by the user)
    hardcodeSpeed       -> the minimum offset in the calculated mouse curcors
                           (set by the user)
    speedDelay          -> the speed in which the mouse cursor shifts (set by the user)
    speedShifter        -> variable that controls the dynamic increase in speed
                          based on users force input (set by the user)
*/
struct essentials
{
  int frontmid, backmid, leftmid, rightmid, deadZone;
  int pressuremid, pressureSensitivity, pressuremax,pressuremin;
  int a1, a2, a3, sip_Hold, hardcodeSpeed, speedDelay, speedShifter;
  int keyboardMode, arrowKeyDelay, arrowSensitivity;
};


void setup()
{
  for (int i = 6; i <= 9; i++)
    pinMode(i, INPUT);
  pinMode(buzzer, OUTPUT);
  
  readRom();
  //check to see if the modeselector button is pressed. If so, start serial command
  if (digitalRead(modeSelector) == HIGH)
  {  SerialConnect();
   
  }
  i2c_scanner();
  Mouse.begin();
  Keyboard.begin();
  beep('s');
  delay(1000);
  
}

void loop()
{
  int xe, ye;
  getRawValues(false);
  getDirectionPressureval(xe, ye);
 while (Serial.available() > 0) {
        // get incoming byte:
        inByte = Serial.read();
        parseByte (inByte);      // implemented in parser.cpp
      }
  
  
  if (keyboardMode)
   {
     if(ye<= -arrowSensitivity )
      Keyboard.press(KEY_UP_ARROW );
     else if(ye>= arrowSensitivity )
      Keyboard.press(KEY_DOWN_ARROW );
     else if(xe<= -arrowSensitivity)
      Keyboard.press(KEY_LEFT_ARROW );
     else if(xe>= arrowSensitivity)
      Keyboard.press(KEY_RIGHT_ARROW );
     Keyboard.releaseAll();
     delay(arrowKeyDelay);
   }
  if (xe==0) moveXcnt=0; 
  if (ye==0) moveYcnt=0; 
  if ( (xe != 0) || (ye != 0))
  {
    if (!scrollMode){
      if (cnt2++%4==0)
        {
          if (xe!=0)
              if (moveXcnt<MOUSE_ACCELDELAY) moveXcnt++; 
                 
          if (ye!=0)
              if (moveYcnt<MOUSE_ACCELDELAY) moveYcnt++;  

          if((moveXcnt <= THRESHOLD ) && (moveYcnt <= THRESHOLD)) Mouse.move(0,0);
          else                
          {Mouse.move(xe * moveXcnt/MOUSE_ACCELDELAY, ye * moveYcnt/MOUSE_ACCELDELAY);
          Serial.println("moveX values... ");Serial.println(xe);Serial.println(moveXcnt);Serial.println(MOUSE_ACCELDELAY); 
          Serial.println("moveY values... ");Serial.println(ye);Serial.println(moveYcnt);Serial.println(MOUSE_ACCELDELAY); }
       }
    }
         
    else
    {
      Mouse.move(0 , 0, (ye > 0) ? -1 : 1);
      delay(300);            // scroll_Delay
    }

    delay(2 * buttondelay);
  }
  
  xe = ye = 0;
  if (sip)
  {
    if (keyboardMode)
    {
      Keyboard.press(KEY_RETURN);
      Keyboard.releaseAll();
      delay(arrowKeyDelay);
    }
    else if (!pTrigger)
    {
      pressureLock = false;
      Mouse.press(MOUSE_RIGHT);
      delay(buttondelay);
      pTrigger = true;
      if (checkForHold('s',1300)&&pTrigger)
      {
          beep('r');
          sip_Hold_Function();
      }
    }
  }

  if (puff)
  {
    if (keyboardMode)
    {
      Keyboard.press(KEY_ESC );
      Keyboard.releaseAll();
      delay(arrowKeyDelay);
    }
    else if (!pTrigger)
    {
      scrollMode = false;
      Mouse.press(MOUSE_LEFT);
      delay(buttondelay);
      pTrigger = true;
      if (checkForHold('p',1300)&&pTrigger)
      {
        pressureLock = true;
        beep('r');
      }
    }
  }
  else
  {
    if (pressureLock == false)
      Mouse.release(MOUSE_LEFT);

    Mouse.release(MOUSE_RIGHT);
    pTrigger = false;
  }
  checkButtons();
  delay(speedDelay);
}


/*******************************************************************************
  Function Name: beep
  parameters: type_of_beep
  Function:
   This function helps produce different type of buzzer beeps for the device,
  depending on the value of the type_of_beep
  s:- Buzz to indicate  initialisation
  e:- Buss to indicate Erasing ROM Value
  l:- Buzz to indicate left click
  r:- Buzz to indicate right click
  i:- Buzz to indicate writing data to ROM
*******************************************************************************/
void beep(char type_of_beep = 's')
{
  if (type_of_beep == 's')                    //initialisation sequence
  {
    tone(buzzer, 1000, 100);
    delay(200);
    tone(4, 1000, 100);
  }
  if (type_of_beep == 'e')                    //erase ROM
    tone(buzzer, 3300, 500);

  if (type_of_beep == 'l')                     //left click
    tone(buzzer, 800, 10);

  if (type_of_beep == 'r')                     //right click
    tone(buzzer, 400, 10);

  if (type_of_beep == 'i')                     //Write Rom Value
  {
    tone(buzzer, 200, 3);
    delay(100);
    tone(buzzer, 300, 3);
    delay(100);
    tone(buzzer, 400, 3);
    delay(100);
    tone(buzzer, 500, 3);
  }
  if(type_of_beep == 'k'){
    tone(buzzer,1000,500);
    delay(100);
    tone(buzzer,500,100);
  }

}

/*******************************************************************************
  Function Name: getRawValues
  parameters: printRaw (default value false)
  Function:
      To fetch the raw value from the force and pressure sensors. Analog value are
    feched from the pins and are mapped to 1/10th scale.The values are stored into
    global variable froy,back,left,right and pressure.
      The parameter printRaw can be set to true, so as to print the values onto
    the serial monitor.
*******************************************************************************/
void getRawValues(bool printRaw = false)
{

  front = analogRead(pin_front); back = analogRead(pin_back); right = analogRead(pin_right); left = analogRead(pin_left);
  pressure = map(analogRead(pin_pressure_sensor), pressuremin, pressuremax, 0, 10);
  if (printRaw == true )
     {printIt("Raw front:",front);printIt(":back:",back);printIt(":left:",left);printIt(":right:",right);Serial.print(":Pressure:");Serial.println(pressure);}
  front = map (front, 100, 1024, 0, 100);
  back  = map (back, 100, 1024, 0, 100);
  left  = map (left, 100, 1024, 0, 100);
  right = map (right, 100, 1024, 0, 100);
  if (printRaw == true)
     {printIt("PRO front:",front);printIt(":back:",back);printIt(":left:",left);printIt(":right:",right,1);}

}

/*******************************************************************************
  Function Name: getDirectionPressureval
  parameters: xe, ye (estimate values for x and y axis)
  Function:
    This function calcultes the final values for the x and y cordinates to which
  the mouse cursor should be moved. The function calculates the final values based
  on the values of the global variables 'front', 'back', 'left', 'right', which
  are mapped by the function 'getRawValues'.
    Thie function also checks whether sip or puff is made based on the values of
  the global variable 'pressure'

Method in Detail:
    THe calculated 'front', 'back', 'right', 'left', and 'pressure' values are
  compared to their calibrated middle (mid) values  and the the preffered deadzone
  applied by the user. This function also helps to increase the speed of the
  cursor if greater force is applied to the lip joystick (this feature can be
  enabled or dissabled by the user through the interface).
*******************************************************************************/
void getDirectionPressureval(int &xe, int &ye)
{
  int temp;
  if (front < midfront - deadZone)
    {
      Serial.println(" > ");Serial.print(midfront);Serial.print(" < midFront||front > ");Serial.print(front);
      temp = (midfront- deadZone)-front;
      if ((temp > speedShifter) && (speedShifter != 0))
        ye = -1 - hardcodeSpeed-((temp>=speedShifter)? temp-speedShifter: speedShifter-temp);
      else
        ye = -1 - hardcodeSpeed;
    }

  else if (back < midback - deadZone)
   {
    Serial.println(" > ");Serial.print(midback);Serial.print(" < midBack||back > ");Serial.print(back);
     temp = (midback- deadZone)- back;
     if ((temp > speedShifter)&& (speedShifter != 0))
        ye = 1 + hardcodeSpeed+((temp>=speedShifter)? temp-speedShifter: speedShifter-temp);
    else
        ye = 1 + hardcodeSpeed;
   }

  if (left < midleft - deadZone)
  {
    Serial.println(" > ");Serial.print(midleft);Serial.print(" < midleft||left > ");Serial.print(left);
    temp = (midleft- deadZone)- left;
    if ((temp > speedShifter)&& (speedShifter != 0))
        xe = -1 - hardcodeSpeed-((temp>=speedShifter)? temp-speedShifter: speedShifter-temp);
    else
        xe = -1 - hardcodeSpeed;
  }

  else if (right < midright - deadZone)
   {
    Serial.println(" > ");Serial.print(midright);Serial.print(" < midRight||Right > ");Serial.print(right);
    temp = (midright- deadZone)- right;
      if ((temp > speedShifter)&& (speedShifter != 0))
        xe = 1 + hardcodeSpeed+((temp>=speedShifter)? temp-speedShifter: speedShifter-temp);
    else
        xe = 1 + hardcodeSpeed;
   }

  if (pressure < (midpressure - Psensitivity))    //SIP
    {sip = true;beep('r');}
  else if (pressure > (midpressure + Psensitivity))    // PUFF
    {puff = true;if (!pressureLock) beep('l');}
  else
    sip = puff=false;

}

/*******************************************************************************
  Function Name: checkButtons
  parameters: nil
  Function:
    Function to rotuinely check the button states of the flipmouse. Function
  checks for mode Selector button, aux (1,2,3) switches. This function also
  helps implement the multi-functionality for the switch by press and hold of
  the mode selector button for 2.3 seconds
    In case of aux switches the function waits till the switch is released.
*******************************************************************************/
void checkButtons()
{
  if (digitalRead(modeSelector) == HIGH) //check for mode selector
  {
    if (!pressed_mode_selector)
    {
      beep('s');
      timer1 = millis();
      pressed_mode_selector = true;
    }
    if (pressed_mode_selector)
    {
      if ((millis() - timer1) > 2300)
      {
        calibrateForceSensors();
      }
    }
  }
  else
    pressed_mode_selector = false;

  if (digitalRead(pin_aux1) == HIGH)
    {
      performFunction(faux1);
      while(digitalRead(pin_aux1) == HIGH);
        //beep('l');
    }

  if (digitalRead(pin_aux2) == HIGH)
    {
      performFunction(faux2);
      while(digitalRead(pin_aux2) == HIGH);
        //beep('l');
    }

   if (digitalRead(pin_aux3) == HIGH)
    {
      performFunction(faux3);
      while(digitalRead(pin_aux3) == HIGH)
       beep('l');
    }

}

/*******************************************************************************
  Function Name: calibrateForceSensors
  parameters: nil
  Function:
    Function to calibrate the force sensors. This process is done by taking the
  present values of the force sensors and pressure sensors and making them the
  mid values. This is done so that getDirectionPressureval function could
  accurately calculate the x,y shift for the mouse cursor.
    The updated values are written diretcly into the ROM by using the function
  'writeRom'
*******************************************************************************/
void calibrateForceSensors()
{
  beep('s');
  delay(500);
  midfront = front - threshold;
  midback = back - threshold;
  midleft = left - threshold;
  midright = right - threshold;
  midpressure = pressure;
  writeRom('c');
  delay(500);
  beep('e');
  pressed_mode_selector = false;
}

/*******************************************************************************
  Function Name: checkForHold
  parameters: type and holdTime
  Function:
    Function that helps to multipile functionality for sip and puff. This feature
  helps to perform an other task if the user sip (or puff) and hold. The function
  looks for pressure change, and for how long does the presure change stays.
    The type variable sets whether if its sip ('s') or puff ('p') that it has to
  monitor and holdTime set for how long the state has to be like that to actuat
*******************************************************************************/
int checkForHold(char type, int holdTime)
{
  timer1 = millis();
  pressure = map(analogRead(pin_pressure_sensor), pressuremin, pressuremax, 0, 10);
  if(type=='s')
    while (pressure < (midpressure - Psensitivity))
      {
        pressure = map(analogRead(pin_pressure_sensor), pressuremin, pressuremax, 0, 10);
        if (millis() - timer1 > holdTime)
          return true;
      }

   if(type=='p')
    while (pressure > (midpressure + Psensitivity))
      {
        pressure = map(analogRead(pin_pressure_sensor), pressuremin, pressuremax, 0, 10);
        if (millis() - timer1 > holdTime)
          return true;
      }
  return false;
}

/*******************************************************************************
  Function Name: printIt
  parameters: ip , data, newLine
  Function:
    The function to print the label and the value through serial monitor in debug
  mode. This function mainly aims to reduce code size, where each and every value
  needs to have atleast two Serial.print() statements to display values.
*******************************************************************************/
void printIt(const char* ip, int &data,bool newLine = 0)
{
  Serial.print(ip);
  newLine?Serial.println(data):Serial.print(data);
}

/*******************************************************************************
 Function to reset arduino when called.
*******************************************************************************/
void(* resetFunc) (void) = 0; // function to reset arduino  //no arguments taken and no value returned

