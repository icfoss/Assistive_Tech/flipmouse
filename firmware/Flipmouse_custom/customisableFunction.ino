/*******************************************************************************
  Function Name: sip_Hold_Function
  parameters: nil
  Function:
    This function is to check for further sip and hold to cross check the entry
  into I2C conect mode. 
*******************************************************************************/
void sip_Hold_Function()
{
  Mouse.release(MOUSE_RIGHT);
  Mouse.move(-10, -2, 0);
  Mouse.press(MOUSE_LEFT);
  Mouse.release(MOUSE_LEFT);
  Mouse.move(-10, 2, 0);

  /*
   * Check for continuous hold of sip to transfer device control
   */
  timer1 = millis();
  if (checkForHold('s',1300))
  {
    beep('e');
    control_transfer_fixDevice();
    beep('e');
    return (1);
   }
  /*
   * Note: Fsip is overwrited here
   */
  fSip_Hold = 0;
  switch (fSip_Hold)
  {
    case 0: // deafult value which denotes scroll function
          scrollMode = true;
          break;
    case 1:
          Keyboard.press(0x7F);
          delay(20);
          Keyboard.releaseAll();
          break;

  }
}

/*******************************************************************************
  Function Name: performFunction
  parameters: integer
  Function:
   This function will connect the aux port actuations to particular HID mouse
  functions.
*******************************************************************************/
void performFunction (int a)
{
  switch (a)
  {
    case 1:Mouse.press(MOUSE_LEFT);
           Mouse.release(MOUSE_LEFT);
           break;
    case 2:Mouse.press(MOUSE_RIGHT);
           Mouse.release(MOUSE_RIGHT);
           break;
    case 3:Keyboard.press(KEY_BACKSPACE);
           Keyboard.release(KEY_BACKSPACE);
           break;
  }
}
