#include "Mouser.h"

const struct atCommandType atCommands[] PROGMEM = {
  {"ID" , PARTYPE_NONE }, {"BM" , PARTYPE_UINT }, {"CL" , PARTYPE_NONE }, {"CR" , PARTYPE_NONE },
  {"CD" , PARTYPE_NONE }, {"CM" , PARTYPE_NONE }, {"HL" , PARTYPE_NONE },  {"HR" , PARTYPE_NONE },
  {"RL" , PARTYPE_NONE }, {"RR" , PARTYPE_NONE }, {"RM" , PARTYPE_NONE }, {"WU" , PARTYPE_NONE }, {"WD" , PARTYPE_NONE }, {"WS" , PARTYPE_UINT }, 
  {"MX" , PARTYPE_INT  }, {"MY" , PARTYPE_INT  }, {"KP" , PARTYPE_STRING },{"SA" , PARTYPE_STRING},
  {"LO" , PARTYPE_STRING},{"LA" , PARTYPE_NONE }, {"LI" , PARTYPE_NONE }, {"NE" , PARTYPE_NONE }, {"DE" , PARTYPE_NONE }, {"NC" , PARTYPE_NONE },
  {"MM" , PARTYPE_UINT }, {"SW" , PARTYPE_NONE }, {"SR" , PARTYPE_NONE }, {"ER" , PARTYPE_NONE }, {"CA" , PARTYPE_NONE },
  {"AX" , PARTYPE_UINT }, {"AY" , PARTYPE_UINT }, {"DX" , PARTYPE_UINT }, {"DY" , PARTYPE_UINT },
  {"MS" , PARTYPE_UINT }, {"AC" , PARTYPE_UINT }, {"MA" , PARTYPE_STRING},{"WA" , PARTYPE_UINT},
  {"TS" , PARTYPE_UINT }, {"TP" , PARTYPE_UINT }, {"SP" , PARTYPE_UINT }, {"SS" , PARTYPE_UINT },
  {"GU" , PARTYPE_UINT}, {"GD" , PARTYPE_UINT },  {"GL" , PARTYPE_UINT }, {"GR" , PARTYPE_UINT },   
  {"RO" , PARTYPE_UINT }, {"BT" , PARTYPE_UINT }, {"FR"  , PARTYPE_NONE }
  
  /**
     * CMD_ID, CMD_BM, CMD_CL, CMD_CR, CMD_CD, CMD_CM, CMD_HL, CMD_HR, CMD_RL, CMD_RR, CMD_RM, CMD_WU, CMD_WD, CMD_WS,
  CMD_MX, CMD_MY, CMD_KP, CMD_SA, CMD_LO, CMD_LA, CMD_LI, CMD_NE, CMD_DE, CMD_NC, CMD_MM, CMD_SW, CMD_SR, CMD_ER, 
  CMD_CA, CMD_AX, CMD_AY, CMD_DX, CMD_DY, CMD_MS, CMD_AC, CMD_MA, CMD_WA, CMD_TS, CMD_TP, CMD_SP, CMD_SS, CMD_GU,
  CMD_GD, CMD_GL, CMD_GR, CMD_RO, CMD_BT, CMD_FR, NUM_COMMANDS
**/
  
 }; //stores in program memory(FLASH)

// 
  // 
 void printCurrentSlot() {
        Serial.print("Slot:");  Serial.println(settings.slotName);
        Serial.print(F("AT AX ")); Serial.println(settings.ax);  //F() used to print to
        Serial.print(F("AT AY ")); Serial.println(settings.ay);
        Serial.print(F("AT DX ")); Serial.println(settings.dx);
        Serial.print(F("AT DY ")); Serial.println(settings.dy);
        Serial.print(F("AT MS ")); Serial.println(settings.ms);
        Serial.print(F("AT AC ")); Serial.println(settings.ac);
        Serial.print(F("AT TS ")); Serial.println(settings.ts);
        Serial.print(F("AT TP ")); Serial.println(settings.tp);
        Serial.print(F("AT WS ")); Serial.println(settings.ws);
        Serial.print(F("AT SP ")); Serial.println(settings.sp);
        Serial.print(F("AT SS ")); Serial.println(settings.ss);
        Serial.print(F("AT MM ")); Serial.println(settings.stickMode);
        Serial.print(F("AT GU ")); Serial.println(settings.gu);
        Serial.print(F("AT GD ")); Serial.println(settings.gd);
        Serial.print(F("AT GL ")); Serial.println(settings.gl);
        Serial.print(F("AT GR ")); Serial.println(settings.gr);
for (int i=0;i<NUMBER_OF_BUTTONS;i++) 
        {
           Serial.print(F("AT BM ")); 
           if (i<9) Serial.print('0');
           Serial.println(i+1); 
           Serial.print(F("AT ")); 
           int actCmd = buttons[i].mode;
           char cmdStr[4];
           strcpy_FM(cmdStr,(uint_farptr_t_FM) atCommands[actCmd].atCmd);
           Serial.print(cmdStr);
            switch (pgm_read_byte_near(&(atCommands[actCmd].partype))) 
            {
               case PARTYPE_UINT: 
               case PARTYPE_INT:  Serial.print(' ');Serial.print(buttons[i].value); break;
               case PARTYPE_STRING: Serial.print(' ');Serial.print(getKeystring(i)); break;
            }
            Serial.println("");
        }
     }

void performCommand(uint8_t cmd, int16_t par1, char * keystring, int8_t periodicMouseMovement)

{
   static uint8_t actButton=0;

    if (actButton != 0)  // if last command was BM (set buttonmode): store current command for this button !!
    {
    if (DebugOutput==DEBUG_FULLOUTPUT) {
          Serial.print(F("got new mode for button ")); Serial.print(actButton);Serial.print(':');
          Serial.print(cmd);Serial.print(',');Serial.print(par1);Serial.print(',');Serial.println(keystring);
        }
        buttons[actButton-1].mode=cmd;
        buttons[actButton-1].value=par1;
        if (keystring==0) setKeystring(actButton-1,"");
        else setKeystring(actButton-1,keystring);
        actButton=0;
        if (DebugOutput==DEBUG_FULLOUTPUT) {
           printKeystrings();
        }
        return;  // do not actually execute the command (just store it)
    }
    
    
 switch(cmd) {
  case CMD_ID:  Serial.println(VERSION_STRING);
              break;
  case CMD_BM: 
               release_all(); 
               if(DebugOutput ==DEBUG_FULLOUTPUT){
                Serial.print(F("set mode for button")); Serial.println(par1);              
               }
               if((par1>0) && (par1 <= NUMBER_OF_BUTTONS))
                actButton = par1;
               else Serial.println("??");
             break; 
  case CMD_CL:  
                mousePress(MOUSE_LEFT);
                 if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("click left"));
                delay(CLICK_TIME);
                mouseRelease(MOUSE_LEFT);
                break;
  case CMD_CR:
                mousePress(MOUSE_RIGHT);
                if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("click right"));
                delay(CLICK_TIME);
                mouseRelease(MOUSE_RIGHT);
                break;
  case CMD_CD: 
               if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("click double"));
              mousePress(MOUSE_LEFT);
              delay(CLICK_TIME);
              mouseRelease(MOUSE_LEFT);                                  
              delay(CLICK_TIME);
              mouseRelease(MOUSE_LEFT);
              delay(CLICK_TIME);
               mouseRelease(MOUSE_LEFT);
               break;
  case CMD_CM:
              mousePress(MOUSE_MIDDLE);
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("click middle"));
              delay(CLICK_TIME);
              mouseRelease(MOUSE_MIDDLE);
              break;
  case CMD_HL:
              mousePress(MOUSE_LEFT);
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("Hold left"));
              break;
  case CMD_HR: 
              mousePress(MOUSE_RIGHT);
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("Hold Right"));
              break;
  case CMD_RL:
              mouseRelease(MOUSE_LEFT); 
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("Release Left"));
              break; 
  case CMD_RR:
              mouseRelease(MOUSE_RIGHT); 
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("Release Right"));
              break; 
  case CMD_RM:
              mouseRelease(MOUSE_MIDDLE); 
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("Release Middle"));
              break; 
  case CMD_WU:
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("wheel up"));
            #ifdef ARDUINO_PRO_MICRO
         if(settings.ws != 0) Mouse.move(0,0,-settings.ws); 
          else Mouse.move(0,0,-DEFAULT_WHEEL_STEPSIZE); 
               #else
          if(settings.ws != 0) Mouse.scroll(-settings.ws); 
          else Mouse.scroll(-DEFAULT_WHEEL_STEPSIZE); 
               #endif
             break;
  case CMD_WD:
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("wheel down"));
             #ifdef ARDUINO_PRO_MICRO
          if(settings.ws != 0) Mouse.move(0,0,settings.ws); 
          else Mouse.move(0,0,DEFAULT_WHEEL_STEPSIZE); 
               #else
          if(settings.ws != 0) Mouse.scroll(settings.ws); 
          else Mouse.scroll(DEFAULT_WHEEL_STEPSIZE); 
            #endif
             break;
  case CMD_WS:
               if (DebugOutput==1)
               Serial.println(F("wheel step"));
             settings.ws=par1;
             break;
  case CMD_MX:
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("Mouse move x"));
             if(periodicMouseMovement) moveX = par1;
             else mouseMove(par1,0);
             break;  
  case CMD_MY:
              if (DebugOutput==DEBUG_FULLOUTPUT)
                 Serial.println(F("Mouse move y"));
             if(periodicMouseMovement) moveY = par1;
             else mouseMove(0,par1);                                                    
             break;
  case CMD_KP:
            if (DebugOutput==1)   
               {  Serial.print(F("key press: ")); Serial.println(keystring); }
            pressSingleKeys(keystring);
            break;
  case CMD_SA:
               if (DebugOutput==1) 
               {  Serial.print(F("save slot "));  Serial.println(keystring); }
             release_all();
             if(keystring) {
              if((strlen(keystring) > 0) && (strlen(keystring) < MAX_NAME_LEN))
              saveToEEPROM(keystring);                                                         
             }
             break;
  case CMD_LO:
              if (DebugOutput==1) 
               {  Serial.print(F("load slot: ")); Serial.println(keystring); }
            if(keystring){
                 release_all();
                 readFromEEPROM(keystring);
                 reportSlotParameters= REPORT_NONE;
                 if((settings.gu != 50) || (settings.gd != 50) || (settings.gr != 50) || (settings.gl !=50))
                 {
                  cx= settings.cx; cy = settings.cy;      
                 }    
            }
             break;
 case CMD_LA:                                             //fabi like
            if (DebugOutput==1) 
                 Serial.println(F("load all slots"));
            release_all();
            reportSlotParameters = REPORT_ALL_SLOTS;
            readFromEEPROM(keystring);
            reportSlotParameters = REPORT_NONE;
            readFromEEPROM(0);
          break;
 case CMD_LI: 
             if (DebugOutput==1) 
                 Serial.println(F("list slots: "));
            release_all();
            listSlots();
           break;
 case CMD_NE:
               if (DebugOutput==1) {
                 Serial.println(F("load next slot"));
                 reportSlotParameters=REPORT_ONE_SLOT;
               }
             release_all();
             readFromEEPROM(0);
             reportSlotParameters=REPORT_NONE;
            break;
 case CMD_DE: 
           if (DebugOutput==1)   
               Serial.println(F("delete slots")); 
            release_all();
            deleteSlots(); 
            break;                                
 case CMD_NC:
            if (DebugOutput==1) 
               Serial.println(F("no command")); 
            break;
 case CMD_MM:
            settings.stickMode = par1;
            if(DebugOutput == DEBUG_FULLOUTPUT){
              if(settings.stickMode == STICKMODE_MOUSE) Serial.println("Mouse activated");
              else Serial.println("Alternate functions activated");           
            }
           break;
 case CMD_SW:
            if(settings.stickMode == STICKMODE_ALTERNATIVE) settings.stickMode = STICKMODE_MOUSE;
            else settings.stickMode = STICKMODE_ALTERNATIVE;
           break;            
 case CMD_SR:
              reportRawValues=1;
            break;
 case CMD_ER:
              reportRawValues=0;
            break;    
 case CMD_CA: 
             calib_now = 100;
           //  makeTone(TONE_CALIB,0);
            break;
 case CMD_AX: 
             if(DebugOutput == 1){
            Serial.println(F("AX: "));       
            }
             settings.ax = par1;
              break;
 case CMD_AY: 
            if(DebugOutput == 1){
            Serial.println(F("AY: "));   
            }
            settings.ay = par1;
           break;
 case CMD_DX:
            if(DebugOutput == 1){
            Serial.println(F("DX: "));  
            }
            settings.dx = par1;
           break;
 case CMD_DY:
            if(DebugOutput == 1){
            Serial.println(F("DY: "));  
            }
            settings.dy = par1;
           break;
 case CMD_MS: 
            if(DebugOutput == 1){
            Serial.println(F("MS: "));  
            }
            settings.ms= par1;
           break;
 case CMD_AC: 
            if(DebugOutput == 1){
            Serial.println(F("AC: "));  
            }
            settings.ac = par1;
           break;
 case CMD_MA:{
                 memmove(cmdstring, keystring, strlen(keystring) + 1);
                 uint8_t lastCmd=0;
                 char * cmdStart=cmdstring, *cmdEnd;

                 // do the macro stuff: feed single commands to parser, separator: ';'
                 do {
                   cmdEnd=cmdStart;
                   while ((*cmdEnd!=';') && (*cmdEnd)) {
                    // use backslash for passing special characters (; or \). note: copy also 0-delimiter! 
                    if (*cmdEnd =='\\') memmove (cmdEnd, cmdEnd+1, strlen(cmdEnd));
                    cmdEnd++;
                   }
                   if (!(*cmdEnd)) lastCmd=1;
                   else *cmdEnd=0; 
                   parseCommand(cmdStart);
                   cmdStart=cmdEnd+1;
                   if (!(*cmdStart)) lastCmd=1;
                 } while (!lastCmd);        
               }
               break;

        case CMD_WA:
                delay(par1);
               break;
        case CMD_TS:
              if(DebugOutput == 1){
                Serial.println(F("TS: "));  
                }
               settings.ts=par1;
            break;
        case CMD_TP:
                if(DebugOutput == 1){
                Serial.println(F("TP: "));  
                }
               settings.tp=par1;
            break;
        case CMD_SP:
               if(DebugOutput == 1){
                Serial.println(F("SP: "));  
                }
               settings.sp=par1;
            break;
        case CMD_SS:
                if(DebugOutput == 1){
                Serial.println(F("SS: "));  
                }
               settings.ss=par1;
            break;
        case CMD_GU:
               if(DebugOutput == 1){
                Serial.println(F("GU: "));  
                 }
               settings.gu=par1;
            break;
        case CMD_GD:
               if(DebugOutput == 1){
                Serial.println(F("GD: "));  
                }
               settings.gd=par1;
            break;
        case CMD_GL:
               if(DebugOutput == 1){
                Serial.println(F("GL: "));  
                }
               settings.gl=par1;
            break;
        case CMD_GR:
                if(DebugOutput == 1){
                 Serial.println(F("GR: "));  
                }
               settings.gr=par1;
              break;
        case CMD_RO:
               if(DebugOutput == 1){
               Serial.println(F("RO: "));  
               }
               settings.ro=par1;
            break;
        case CMD_BT:
                if(DebugOutput == 1){
                 Serial.println(F("BT: "));  
                    }
               settings.bt=par1;
               
            break;                    
        case CMD_FR:                              //fabi like
             Serial.print(F("FREE EEPROM (%):"));
             Serial.println((int)((uint32_t) freeEEPROMbytes * 100 / EEPROM_SIZE));
              break;                                                                                            
   }

 
  
}

























       
      
