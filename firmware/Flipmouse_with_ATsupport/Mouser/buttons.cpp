#include "Mouser.h"

#define DEFAULT_DEBOUNCING_TIME 7

struct slotButtonSettings buttons[NUMBER_OF_BUTTONS];
char * keystringButtons[NUMBER_OF_BUTTONS];
char keystringBuffer[MAX_KEYSTRINGBUFFER_LEN];
uint16_t keystringBufferLen=0;
struct buttonDebouncerType buttonDebouncers[NUMBER_OF_BUTTONS];


/**uint16_t deleteKeystringButton(uint8_t buttonIndex)
{
   char * startaddress;
   uint16_t len;
   startaddress = keystringButtons[buttonIndex];
   if (!startaddress) return(0);
   len = strlen(startaddress)+1;
   // Serial.print("delete:");Serial.println(startaddress);

   uint16_t num=keystringBuffer+MAX_KEYSTRINGBUFFER_LEN-(startaddress+len);
   memmove(startaddress, startaddress+len, num);
   keystringButtons[buttonIndex]=0;
   for (int i=0;i<NUMBER_OF_BUTTONS;i++)    
   {
      if (keystringButtons[i]>startaddress) keystringButtons[i]-=len;
   }
   keystringBufferLen-=len;

   //Serial.print("bytes deleted:");Serial.println(len);
   //Serial.print("bytes left:");Serial.println(MAX_KEYSTRINGBUFFER_LEN-keystringBufferLen);

   return(MAX_KEYSTRINGBUFFER_LEN-keystringBufferLen);
}**/

/**uint16_t storeKeystringButton(uint8_t buttonIndex, char * text)
{
   char * targetaddress;
   deleteKeystringButton(buttonIndex);
   if (keystringBufferLen + strlen(text) >= MAX_KEYSTRINGBUFFER_LEN-1) return(0);
   targetaddress=keystringBuffer+keystringBufferLen;
   strcpy (targetaddress, text);
   keystringButtons[buttonIndex]=targetaddress;
   keystringBufferLen+=strlen(text)+1;
   // Serial.print("allocated stringbuffer:");Serial.println(keystringButtons[buttonIndex]);
   // Serial.print("bytes left:");Serial.println(MAX_KEYSTRINGBUFFER_LEN-keystringBufferLen);
   return(MAX_KEYSTRINGBUFFER_LEN-keystringBufferLen);
}**/

void initButtons() {
     buttons[0].mode=CMD_NE;  // default function for first button: switch to next slot
     buttons[1].mode=CMD_NC;  // no command
     buttons[2].mode=CMD_NC;  // no command
     buttons[3].mode=CMD_KP; setKeystring(3,"KEY_UP ");
     buttons[4].mode=CMD_KP; setKeystring(4,"KEY_DOWN ");
     buttons[5].mode=CMD_KP; setKeystring(5,"KEY_LEFT ");
     buttons[6].mode=CMD_KP; setKeystring(6,"KEY_RIGHT ");
     buttons[7].mode=CMD_HL;   // hold left mouse button
     buttons[8].mode=CMD_NC;   // no command 
     buttons[9].mode=CMD_CR;   // click right                        
     buttons[10].mode=CMD_CA;  // calibrate      
     buttons[11].mode=CMD_NC;  // no command      
     buttons[12].mode=CMD_NC;      
     buttons[13].mode=CMD_NC;      
     buttons[14].mode=CMD_NC;      
     buttons[15].mode=CMD_NC;      
     buttons[16].mode=CMD_NC;      
     buttons[17].mode=CMD_NC;      
     buttons[18].mode=CMD_NC;      
}

void handlePress(int buttonIndex)
{
  performCommand(buttons[buttonIndex].mode, buttons[buttonIndex].value,getKeystring(buttonIndex),1);
  
}

void handleRelease(int buttonIndex){

  switch(buttons[buttonIndex].mode){
    case CMD_HL: 
                mouseRelease(MOUSE_LEFT);
                break;
    case CMD_HR:
                mouseRelease(MOUSE_RIGHT);
                break;            
    case CMD_MX:
                moveX = 0;
                break;
    case CMD_MY: 
                moveY = 0;
                break;              
    case CMD_KP: releaseSingleKeys(getKeystring(buttonIndex)); break; 
    
  }
}

uint8_t handleButton(int i, uint8_t state){
if(buttonDebouncers[i].bounceState == state){
    if(buttonDebouncers[i].bounceCount < DEFAULT_DEBOUNCING_TIME){
      buttonDebouncers[i].bounceCount++;
      if(buttonDebouncers[i].bounceCount == DEFAULT_DEBOUNCING_TIME){
        if(state != buttonDebouncers[i].stableState){

          buttonDebouncers[i].stableState = state;
          if(state == 1){
            handlePress(i);
            buttonDebouncers[i].timestamp = millis();
          }
          else{
            if(inHoldMode(i))
            handleRelease(i);
            return(1);
          }
        }
        
      }
    }
    else{ }
  }
  else {
     buttonDebouncers[i].bounceState = state;
     buttonDebouncers[i].bounceCount=0;     
   }
   return(0);
  
}

uint8_t inHoldMode(int i){

if( (buttons[i].mode == CMD_MX) ||
    (buttons[i].mode == CMD_MY) ||
    (buttons[i].mode == CMD_HL) || 
    (buttons[i].mode == CMD_HR) )
return(1);
else return(0);  
}
void initDebouncers()
{
   for (int i=0; i<NUMBER_OF_BUTTONS; i++)   // initialize button array
   {
      buttonDebouncers[i].bounceState=0;
      buttonDebouncers[i].stableState=0;
      buttonDebouncers[i].bounceCount=0;
      buttonDebouncers[i].longPressed=0;
   }
}




