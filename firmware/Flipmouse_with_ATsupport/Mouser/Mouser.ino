/*  Firmware for  FlipMouse 
    
    Copyright (C) 2020  ICFOSS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/******************* PS: This firmware is programmed to work with Arduino Promicro ******************
                         All configurations are done using Asterics Gui
 ****************************************************************************************************/                                                

#include "Mouser.h"
#include <EEPROM.h>
#include <Wire.h>
#include "math.h"

#define DEFAULT_WAIT_TIME    5

//global variables

#define DOWN_SENSOR_PIN      A3
#define LEFT_SENSOR_PIN      A2
#define RIGHT_SENSOR_PIN     A1
#define UP_SENSOR_PIN        A0
#define PRESSURE_SENSOR_PIN  A10
#define BUZZER  4

#ifdef ARDUINO_PRO_MICRO
int8_t modeSelector = 7;            //internal pin...used for bluetooth and i2c purpose
int8_t input_map[NUMBER_OF_PHYSICAL_BUTTONS] = {6,8,9};
//int8_t led_map[NUMBER_OF_LEDS] = {1};
#endif


struct slotGeneralSettings settings = {
  "slot1",
  1,                                    //stickmode
  60, 60, 20, 20, 50, 50,             //accx, accy, deadzone x, deadzone y, maxspeed, accel time
  400, 600, 3,                          //threshold sip, threshold puff, wheel step   
  800, 10,                              //threshold strong puff, strong sip  
  50, 50, 50, 50,                        //gain up/down/left/right
  0, 0,                                  //offset x/y
  0,                                      //orientation
  1,                                     //bt-mode  1:USB                        
  
};

//struct slotButtonSettings buttons[NUMBER_OF_BUTTONS];
  
uint8_t workingmem[WORKINGMEM_SIZE];
char   cmdstring[MAX_CMDLEN];  

bool i2cPresent = false;

char slotName[MAX_NAME_LEN] = "empty";
uint8_t reportSlotParameters = REPORT_NONE;
uint8_t reportRawValues = 0;
uint8_t actSlot = 0;
uint16_t calib_now = 1;

uint8_t DebugOutput =   DEBUG_FULLOUTPUT;//   CHANGE THIS TO change response 

int waitTime = DEFAULT_WAIT_TIME;
unsigned long updateStandaloneTimestamp;

int up,down,left,right,tmp;
int x,y;
int pressure;
double dz=0 , force=0, angle=0;

int16_t cx=0, cy=0;
int inByte = 0;
//char * keystring = 0;

//func declarations......
void reportValues();
void applyDeadzone();


extern uint8_t StandAloneMode = 1; //enabled for supporting AT-command


 

void setup() {
  // put your setup code here, to run once:
Serial.begin(115200);
if(DebugOutput == DEBUG_FULLOUTPUT)
  Serial.println("Flipmouse started");

#ifdef ARDUINO_PRO_MICRO   // only needed for Arduino, automatically done for Teensy(duino)
     Mouse.begin();
     Keyboard.begin();
#endif  
  
for(int i=0; i<NUMBER_OF_PHYSICAL_BUTTONS; i++)
pinMode(input_map[i], INPUT_PULLUP);

release_all();
initDebouncers();
for (int i=0; i<NUMBER_OF_BUTTONS; i++)   // initialize button array
   {
      
      buttons[i].mode=0;              // default command for every button is left mouse click           
      buttons[i].value= 0;          // 
      keystringBuffer[i]=0;
      //keystringBufferLen=0;
   }
initButtons();

readFromEEPROM(0);
if(DebugOutput== DEBUG_FULLOUTPUT){
  Serial.print("Free RAM: "); Serial.println(freeRam()); 
}
beep('s');
i2c_scanner();
updateStandaloneTimestamp = millis();
  
}

void loop() {
  // put your main code here, to run repeatedly:
  pressure = analogRead(PRESSURE_SENSOR_PIN);

  up = (uint16_t)((uint32_t)analogRead(UP_SENSOR_PIN) * settings.gd/50); if(up>1023) up = 1023; if(up<0) up=0;
  down=(uint16_t)((uint32_t)analogRead(DOWN_SENSOR_PIN) * settings.gu/50); if(down>1023) down = 1023; if(down<0) down=0;
  left=(uint16_t)((uint32_t)analogRead(LEFT_SENSOR_PIN) * settings.gr/50); if(left>1023) left = 1023; if(left<0) left=0;
  right=(uint16_t)((uint32_t)analogRead(RIGHT_SENSOR_PIN) * settings.gl/50); if(right>1023) right = 1023; if(right<0) right=0; 

switch(settings.ro){
  case 90:  tmp=up; up=left; left=down; down=right; right=tmp; break;
  case 180: tmp=up; up=down; down=tmp; tmp=right; right=left; left=tmp; break;
  case 270: tmp=up; up=right; right=down; down=left; left=tmp; break;
}

while(Serial.available() > 0){
  inByte = Serial.read();
  parseByte(inByte);
}

if(StandAloneMode && (millis() >= updateStandaloneTimestamp+waitTime)){

  updateStandaloneTimestamp = millis();
  if(calib_now  == 0){
    x = (left-right) - cx;
    y = (up-down) -cy;  
  }
  else{
      calib_now--;
      if(calib_now == 0){
        settings.cx = (left-right);
        settings.cy = (up-down);
        cx = settings.cx;
        cy = settings.cy;
      }
  }
    reportValues();
    applyDeadzone();
    handleModeState(x, y , pressure);
  
  }
}

void reportValues(){
  static uint8_t valueReportCount = 0;
  if(!reportRawValues)  return;

  if(valueReportCount++ > 10){
      Serial.print("VALUES:");Serial.print(pressure);Serial.print(",");
      Serial.print(up);Serial.print(",");Serial.print(down);Serial.print(",");
      Serial.print(left);Serial.print(",");Serial.print(right);Serial.print(",");
      Serial.print(x);Serial.print(",");Serial.println(y);
      /*
      Serial.print("AnalogRAW:");
      Serial.print(analogRead(UP_SENSOR_PIN));
      Serial.print(",");
      Serial.print(analogRead(DOWN_SENSOR_PIN));
      Serial.print(",");
      Serial.print(analogRead(LEFT_SENSOR_PIN));
      Serial.print(",");
      Serial.println(analogRead(RIGHT_SENSOR_PIN));
      */
      valueReportCount=0;
  }
}

void applyDeadzone(){
  if(settings.stickMode == STICKMODE_ALTERNATIVE) {
    if(x< settings.dx) x+=settings.dx;
    else if(x> settings.dy) x-=settings.dx;
    else x=0;

    if(y< settings.dy) y+=settings.dy;
    else if(y> settings.dy) y-=settings.dy;
    else y=0;
  }
  else {
    double x2,y2;
    char str[80];

    force = sqrt(x*x + y*y);
    if(force != 0){
      angle = atan2 ((double)y/force, (double)x/force);
      dz = settings.dx * (fabs((double)x)/force)  +   settings.dy * (fabs((double)y)/force);
    }
    else{
      angle = 0; dz = settings.dx;
    }
    if(force<dz) force = 0; else force -= dz;

    y2 = force*sin(angle);
    x2 = force*cos(angle);
    x= int(x2);
    y= int(y2);
  }
}

void release_all(){
  //release_all_keys();
  Keyboard.releaseAll();
  mouseRelease(MOUSE_LEFT);
  mouseRelease(MOUSE_MIDDLE);
  mouseRelease(MOUSE_RIGHT);
  moveX = 0;
  moveY = 0;
}
int freeRam(){
   extern int __heap_start, *__brkval;
    int v;
    return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}
uint16_t keystringMemUsage(uint8_t button)
{
  uint16_t sum=0;
  for (int i=button; i<NUMBER_OF_BUTTONS;i++)
    sum+=strlen(getKeystring(i))+1;
  return(sum);
}
char * getKeystring (uint8_t button) 
{
  char *s=keystringBuffer;
  for (int i=0;i<button;i++)
  {
    while (*s) s++;
    s++;    
  }
  return(s);
}
void setKeystring (uint8_t button, char * text)
{
  if (keystringMemUsage(0)-strlen(getKeystring(button))+strlen(text) >= MAX_KEYSTRINGBUFFER_LEN)
     return;

  if (button < NUMBER_OF_BUTTONS-1) {
    uint16_t bytesToCopy=keystringMemUsage(button+1);
    int16_t delta = strlen(text)-strlen(getKeystring(button));
    memmove(getKeystring(button+1)+delta, getKeystring(button+1), bytesToCopy);
  }
  strcpy(getKeystring(button),text);
}
void printKeystrings ()
{
  Serial.print("Used RAM for Keystrings:");Serial.print(keystringMemUsage(0));
  Serial.print(" (free: ");Serial.print(MAX_KEYSTRINGBUFFER_LEN-keystringMemUsage(0));
  Serial.println(")");

  for (int i=0;i<NUMBER_OF_BUTTONS;i++) {
    Serial.print("Keystring ");Serial.print(i);Serial.print(":");Serial.println(getKeystring(i));
  }
}
void beep(char type_of_beep = 's')
{
  uint8_t buzzer = BUZZER;
  
  if (type_of_beep == 's')                    //initialisation sequence
  {
    tone(buzzer, 1000, 100);
    delay(200);
    tone(4, 1000, 100);
  }
  if (type_of_beep == 'e')                    //erase ROM
    tone(buzzer, 3300, 500);

  if (type_of_beep == 'l')                     //left click
    tone(buzzer, 800, 10);

  if (type_of_beep == 'r')                     //right click
    tone(buzzer, 400, 10);

  if (type_of_beep == 'i')                     //Write Rom Value
  {
    tone(buzzer, 200, 3);
    delay(100);
    tone(buzzer, 300, 3);
    delay(100);
    tone(buzzer, 400, 3);
    delay(100);
    tone(buzzer, 500, 3);
  }
}
