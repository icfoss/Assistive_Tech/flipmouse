#ifndef _HID_HAL_H_
#define _HID_HAL_H_

#include "Mouser.h"

void mousePress(uint8_t button);

void mouseRelease(uint8_t button);

//void mouseScroll(int8_t button);

void mouseMove(int x, int y);


#endif
