FlipMouse Firmware
====================


The FlipMouse firmware is written in Arduino, so that a larger community will be
able to tweak into the firmware and make upgradations. To start with we have 5
different files that help us compile the whole firmware

- **FlipMouse.ino**: *The main file that contains the setup() and loop().*
- **ROM.ino**: *contains function that read, write and validate rom data*
- **i2c.ino**: *contains functions that scans and transfer data to device in I2C bus*
- **serialConnect.into**: *contains functions to connect and acquire data from
calibration tool*
- **customisableFunction.ino** *:contains functions that controls auxports and sip_Hold Functions*

The firmware is divided into different files, so that it will be easy for the users
in the community start tweaking.

## Program Flow

The program flow for the FlipMouse might be confusing for some. The diagram below
might be able to get you over the initial hurdles.

![workflow diagram](images/ProgramFlowChart.png)

## ROM Data
User's custom settings are stored in the onboard ROM. If the flipmouse boots up
for the first time, it sets in factory set value for the custom settings. The
data stored on the rom include:

```
  int front, midfront, back, midback, left, midleft, right, midright, deadZone;
  float pressure; int midpressure, Psensitivity, pressuremax,pressuremin;
  int a1, a2, a3, sip_Hold, hardcodeSpeed, speedDelay, speedShifter;
  int keyboardMode, arrowKeyDelay, arrowSensitivity;

```
These values are updated in the time boot. They are initialised from the onboard
ROM data.
    - * mid               -> middle values of each force and pressure sensors
                             (calculated during the calibration process).

    - deadZone            -> the deadzone for each force sensor (set by the user
                             through adjusting sensivtivity).

    - pressureSensitivity -> The sensitivity of the pressure sensor (set by the user)

    - pressuremax/min     -> The max value of the pressure sensor (set during the
                             sip and puff calibration)

    - a *                 -> The functon value of each aux port (set by the user)

    - hardcodeSpeed       -> The minimum offset in the calculated mouse curcors
                            (set by the user)

    - speedDelay          -> The speed in which the mouse cursor shifts (set by
                             the user)

    - speedShifter        -> Variable that controls the dynamic increase in speed
                              based on users force input (set by the user)

## Serial Connect Mode
This mode is invoked if the mode selector switch is pressed while in boot. This
mode is used to configure the flipmouse custom values. It connects across a host
program in your computer (Serial Monitor in Arduino IDE can also be used) at a
baudrate of 115200. There is a connection sequence that has to be followed.

![Serial mode diagram](images/SerialModeFlowChart.png)

To connect with the 0FlipMouse, first send '!#!' through the connected serial port.
Once connected with the FlipMouse, to send commands, follow the instruction below.

```
command format -  *#*_ _ _ _
```

The commands are recieved as character string with maximum number of 6 characters.
Every command should start with ``` *#*``` and the length of the command can be 4
or 6 depending upon the number of arguments in the commands.

The 3rd and 4th bits conveys the command type and 5th and 6th bit are bits that
pass the value of any particular variable in the command.   

#### lOOK UP TABLE

Function                  |Description |  Command
-----------------------   |------------|----------
Sipp and Puff Calibration |Calibrate sip and puff sensitivity|  ``` *#*sp   ```
ROM Clear                 |Clear ROM data to reset device|  ``` *#*ro   ```
Update ROM                |Update ROM, after inserting new value through command|  ``` *#*ur   ```
Test Sensor Data          |Print force and pressure sensor data through serial port continuously|  ``` *#*te   ```
Reset Device              |To restart device (doesn't clear ROM or change any values internally )|  ``` *#*re   ```
Force calibration         |Calibrate force sensor offset values|  ``` *#*fc   ```
Raw-value OUTPUT |Print raw values from the sensor through serial port |  ``` *#*rv   ```dynamic speed shift
Variable Direct Entry     |To insert value directly into variable. 5th bit is variable details and 6th bit is value|  ``` *#*va_ _```
. | **Force Sensor Sensitivity** to set the sensitivity of the force sensors. The 6th bit is contain the value of sensitivity between 0 -10  |  ``` *#*vaf_   ```
. | **Pressure Sensor Sensitivity** to set the sensitivity of the pressure sensors. The 6th bit contains the value of sensitivity between 0 -5  |  ``` *#*vap_   ```
. | **Set Minimum Speed** to set the minimum speed of mouse cursor. The 6th bit is fixes the speed of the mouse cursor (a value between 0-5)  |  ``` *#*vas_   ```
. | **Set Minimum Pixel Shift** to set the minimum pixel shift of the mouse cursor. The 6th bit is fixes the minimum number pixels to be shift (a value between 0-5)  |  ``` *#*vah_  ```
. | **Set Dynamic Speed Offset** to set the minimum offset over which the speed of the mouse cursor increases with . The 6th bit is fixes the speed of the mouse cursor (a value between 0-5). If value set to 0 dynamic speed shift is off.  |  ``` *#*vad_ ```
Auxilary Port Function |To set the function to be triggered when any of the 3 axulary ports are triggered. Each of the 3 auxilary ports can have different functiona attached to it. Here the 4th bit should be a value between (1 -3) denoting the aux port and the 5th bit should be a charcter. The value of 5th bit can be **a** -> left click, **b** -> Right Click, **c**-> Backspace. ***For example:*** if you want to set the first aux port as right click the command should be ``` *#*a1b``` and if the 2nd aux has to be left click the command should be ``` *#*a2a``` |  ``` *#*a_```
Sip Hold Function Setting | To change the function triggered when the user sip and hold for 3 seconds. Is is by default set to scroll Mode.***( At the time of development the no other options are developed, but a fine tweak with the firmware will help you with it. )*** |  ``` *#*pu_```

## I2C Connect mode
This mode is intiated when you sip and hold for more than about 3 seconds. To be
precise if you sip and hold for 1500 seconds, you get into scroll mode (or any
other customised mode) and if you still hold the sip for the next 1300 seconds
the device control sent across USB as HID is shifted to I2C devices.

![I2CconnectMode flow chart](images/I2CconnectMode.png)

> The above attached images is a cut out from the [work flow diagram](https://gitlab.com/icfoss/Assistive_Tech/flipmouse/tree/master/firmware/Flipmouse#program-flow) attached above.

The FlipMouse scans for I2C devices on boot and stores it in an array. When in
I2C connect mode, the device first takes the input from the lipstick to select
the device from the array created on boot.  If the device is **FlipMouse Compatible**
(i.e if it uses the [FlipMouse I2C Message Parser Library](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-i2c-message-parser-library) )
while traversing through different addresses in the array, the corresponding
[Connect LED](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-i2c-message-parser-library#step-1) of the device lights up to notify the user, which device is presently selected. To select the device just puff into the FlipMouse.

Once the device is selected the data from the force sensors and the pressure sensors
are sent to the I2C devices. To exit the mode, sip and hold for about 1500 seconds and then you go back to HID mode.
