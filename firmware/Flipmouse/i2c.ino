/*
 * Send 'p' to switch on the indicator led
 * Send 's' to switch off the indicator led
 * send "#Xval@Yval" to send cordinates
 */
void i2c_scanner()
{
  Wire.begin();
  //Serial.begin(115200);
  int arrayIndex=0;
  for (int addr = 0; addr < 20; addr++)
  {
      Wire.beginTransmission(addr);
      Wire.write('s');
      if((Wire.endTransmission() == 0) && (addr!=0))
      {
        i2cPresent = true;
        i2cAddr[arrayIndex++] = addr;
        //Serial.println(addr);
      }
  }
 Wire.endTransmission(); 
}

void control_transfer_fixDevice()
{
  int arrayIndex = 0;
  if(!i2cPresent)
    return 0;
  //Serial.print("Waiting to select port!");
  //delay(1000);
  //for(int i=0;i<10;i++)
  //  Serial.print(i2cAddr[i]);
  while(true)
  {
    int xe, ye;
    getRawValues(false);
    getDirectionPressureval(xe,ye);
    if (ye > 0)
      {
        Wire.beginTransmission(i2cAddr[arrayIndex]);  
        Wire.write('s');
        Wire.endTransmission();
        ++arrayIndex;
        while(i2cAddr[arrayIndex]==0)
        {
          arrayIndex++;
          if(arrayIndex>9)
            arrayIndex = 0; 
        }
        Wire.beginTransmission(i2cAddr[arrayIndex]);  
        Wire.write('p');
        Wire.endTransmission();
        //Serial.print("Selected Addr:");
        //Serial.println(i2cAddr[arrayIndex]);
        //Serial.print("Array Index: ");
        //Serial.println(arrayIndex);
        delay(1000);
      }  
    if (pressure > (midpressure + Psensitivity))
    {
      beep('r');
      break; 
    }
  }
  //Serial.print("Connecting to addr: ");
  //Serial.println(i2cAddr[arrayIndex]);
  i2c(i2cAddr[arrayIndex]);
}

void i2c(int addr)
{
  Wire.begin();
  int xe, ye;
  beep('r');
  int a = 0;
  timer1 = millis();
  while (millis()-timer1<2000)
  { 
    getRawValues(false);
    getDirectionPressureval(xe,ye);
    if(pressure > (midpressure - Psensitivity))
      timer1 = millis();
      
    /*
     * Send sip and puff value over i2c
     *  mtehod->  *Val
     *  val = 0 (not sip nor puff)
     *      = 1 (sip)
     *      = 2 (puff)
     */
    Wire.beginTransmission(addr);
    Wire.write('*');
    Wire.endTransmission();
    Wire.beginTransmission(addr);
    if (sip)  Wire.write(1);
    else if ((puff)&&(millis()-timer1<250))  Wire.write(2);
    else Wire.write(0);
    Wire.endTransmission();

    /*
     * Send x axis shift over through i2c 
     * method->  #val
     * send val as val+100 as i2c communication is in 8 bytes
     * so cannot send 16 bytes value. The values will be in 
     * between -100 and 100. Hence we will add a 100 to the   
     * original value, which will be subtracted in the other end
     */
     
    Wire.beginTransmission(addr);
    Wire.write('#');
    Wire.endTransmission();
    Wire.beginTransmission(addr);
    Wire.write(xe+100);
    Wire.endTransmission();
    Wire.beginTransmission(addr);

     /*
     * Send x axis shift over through i2c 
     * method->  @val
     * send val as val+100 as i2c communication is in 8 bytes
     * so cannot send 16 bytes value. The values will be in 
     * between -100 and 100. Hence we will add a 100 to the   
     * original value, which will be subtracted in the other end
     */
    Wire.write('@');
    Wire.endTransmission();
    Wire.beginTransmission(addr);
    Wire.write(ye+100);
    Wire.endTransmission();
  }
  Wire.beginTransmission(addr);
  Wire.write('s');
  Wire.endTransmission();
}

