EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:FlipMousePressurePad-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X05 P3
U 1 1 5AF96013
P 3200 2350
F 0 "P3" H 3200 2650 50  0000 C CNN
F 1 "CONN_01X05" V 3300 2350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05" H 3200 2350 50  0001 C CNN
F 3 "" H 3200 2350 50  0000 C CNN
	1    3200 2350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2100 2650 3000 2650
Wire Wire Line
	3000 2650 3000 2550
Wire Wire Line
	3100 2550 3100 3450
Wire Wire Line
	3200 2550 3200 3000
Wire Wire Line
	3200 3000 4050 3000
Wire Wire Line
	3300 2550 3300 2900
Wire Wire Line
	3300 2900 4600 2900
Wire Wire Line
	3400 2650 3400 2550
$Comp
L CONN_01X01 PA1
U 1 1 5AF97085
P 1600 3650
F 0 "PA1" H 1600 3750 50  0000 C CNN
F 1 "CONN_01X01" V 1700 3650 50  0000 C CNN
F 2 "Measurement_Points:Test_Point_Keystone_5019_Minature" H 1600 3650 50  0001 C CNN
F 3 "" H 1600 3650 50  0000 C CNN
	1    1600 3650
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 PA2
U 1 1 5AF970E2
P 2100 3650
F 0 "PA2" H 2100 3750 50  0000 C CNN
F 1 "CONN_01X01" V 2200 3650 50  0000 C CNN
F 2 "Measurement_Points:Test_Point_Keystone_5019_Minature" H 2100 3650 50  0001 C CNN
F 3 "" H 2100 3650 50  0000 C CNN
	1    2100 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 3450 1350 3450
Wire Wire Line
	1350 3450 1350 3950
Wire Wire Line
	2100 2650 2100 3450
$Comp
L CONN_01X01 PB1
U 1 1 5AF97229
P 2600 3650
F 0 "PB1" H 2600 3750 50  0000 C CNN
F 1 "CONN_01X01" V 2700 3650 50  0000 C CNN
F 2 "Measurement_Points:Test_Point_Keystone_5019_Minature" H 2600 3650 50  0001 C CNN
F 3 "" H 2600 3650 50  0000 C CNN
	1    2600 3650
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 PB2
U 1 1 5AF97261
P 3100 3650
F 0 "PB2" H 3100 3750 50  0000 C CNN
F 1 "CONN_01X01" V 3200 3650 50  0000 C CNN
F 2 "Measurement_Points:Test_Point_Keystone_5019_Minature" H 3100 3650 50  0001 C CNN
F 3 "" H 3100 3650 50  0000 C CNN
	1    3100 3650
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 PC1
U 1 1 5AF97290
P 3600 3650
F 0 "PC1" H 3600 3750 50  0000 C CNN
F 1 "CONN_01X01" V 3700 3650 50  0000 C CNN
F 2 "Measurement_Points:Test_Point_Keystone_5019_Minature" H 3600 3650 50  0001 C CNN
F 3 "" H 3600 3650 50  0000 C CNN
	1    3600 3650
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 PC2
U 1 1 5AF972C2
P 4050 3650
F 0 "PC2" H 4050 3750 50  0000 C CNN
F 1 "CONN_01X01" V 4150 3650 50  0000 C CNN
F 2 "Measurement_Points:Test_Point_Keystone_5019_Minature" H 4050 3650 50  0001 C CNN
F 3 "" H 4050 3650 50  0000 C CNN
	1    4050 3650
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 PD1
U 1 1 5AF972F5
P 4600 3650
F 0 "PD1" H 4600 3750 50  0000 C CNN
F 1 "CONN_01X01" V 4700 3650 50  0000 C CNN
F 2 "Measurement_Points:Test_Point_Keystone_5019_Minature" H 4600 3650 50  0001 C CNN
F 3 "" H 4600 3650 50  0000 C CNN
	1    4600 3650
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 PD2
U 1 1 5AF9735F
P 5150 3700
F 0 "PD2" H 5150 3800 50  0000 C CNN
F 1 "CONN_01X01" V 5250 3700 50  0000 C CNN
F 2 "Measurement_Points:Test_Point_Keystone_5019_Minature" H 5150 3700 50  0001 C CNN
F 3 "" H 5150 3700 50  0000 C CNN
	1    5150 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	1350 3950 5450 3950
Wire Wire Line
	5450 3950 5450 3450
Wire Wire Line
	5150 2650 5150 3500
Wire Wire Line
	5450 3450 5150 3450
Connection ~ 5150 3450
Wire Wire Line
	2600 3450 2350 3450
Wire Wire Line
	2350 3450 2350 3950
Connection ~ 2350 3950
Wire Wire Line
	3600 3450 3350 3450
Wire Wire Line
	3350 3450 3350 3950
Connection ~ 3350 3950
Wire Wire Line
	4050 3000 4050 3450
Wire Wire Line
	4600 2900 4600 3450
Wire Wire Line
	3400 2650 5150 2650
$EndSCHEMATC
